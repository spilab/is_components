import sqlite3
from sqlite3 import Error
import os
import secrets

UPLOAD_DIRECTORY = "resources/"
DB_PATH = "bd.sqlite3"


def get_db():
    print("Getting db connection...")
    return sqlite3.connect(DB_PATH)

def close_connection(db):

    print("Closing db connection...")
    if db is not None:
        db.close()


def getResourcePath(fileid, token):
    print("INFO: Getting resource..")
    conn = get_db()
    cur = conn.cursor()
    cur.execute("SELECT * FROM Resources WHERE fileid=? AND token=?",(fileid,token,))

    rows = cur.fetchall()
    close_connection(conn)
    if(len(rows)>1):
        return None
    elif(len(rows)==0):
        return None

    row= rows[0]

    return row[2]


def saveResource(filename, data):

    print("INFO: Saving resource..")
    conn = get_db()
    cur = conn.cursor()

    token = generateToken()
    fileid = generateToken()
    os.mkdir(os.path.join(UPLOAD_DIRECTORY, fileid))
    path = os.path.join(UPLOAD_DIRECTORY, fileid, filename)


    try:
        cur.execute("INSERT INTO Resources (fileid,token,path)VALUES (?,?,?)", [fileid,token,path])
        conn.commit()
        close_connection(conn)
        try:
            with open(path, "wb") as fp:
                fp.write(data)
            return True, fileid, token
        except Error as e:
            print(e)
    except Error as e:
        close_connection(conn)
        print(e)

    return False, "", ""

def generateToken():
    #each byte is 1.3 characters aprox. To get 6 characters we use 4 bytes tokens
    return secrets.token_urlsafe(nbytes=4)
