import os

from flask import Flask, request, abort, jsonify, send_from_directory
from flask_cors import CORS

import bd_functions as BD



HOST = "localhost"
PORT = 38383

if not os.path.exists(BD.UPLOAD_DIRECTORY):
    os.makedirs(BD.UPLOAD_DIRECTORY)


api = Flask(__name__)
CORS(api, expose_headers=["Content-Disposition"])


@api.route("/files/<fileid>")
def get_file(fileid):

    try:
        token = request.headers['Authorization']
    except Exception as e:
        return "",401

    if token == None:
        return 404
    else:
        path = BD.getResourcePath(fileid,token)
        if path == None:
            return "",404
        else:
            print("Enviando",str(path))
            return send_from_directory("", path, as_attachment=True)



@api.route("/files/<filename>", methods=["POST"])
def post_file(filename):
    data = request.data

    added, fileid, token = BD.saveResource(filename, data)

    data = {"fileid": fileid,"token": token}
    if added:
        return jsonify(data), 201
    else:
        return "",500


if __name__ == "__main__":
    api.run(debug=True, host=HOST, port=PORT)
