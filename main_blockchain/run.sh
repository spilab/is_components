
echo "Introduce el nombre del nodo que vas a desplegar (si es el primero de la red, debe ser node0): "
read nombre

echo "Introduce el puerto para las consultas Torii: "
read puerto_torii

echo "Introduce el puerto interno para el nodo de blockchain: "
read puerto_interno



#Revisar el nombre, el puerto y el blockstore
docker run --name iroha -d -p $puerto_torii:50051 -p $puerto_interno:10001 -v $(pwd)/iroha/example:/opt/iroha_data -v blockstore:/tmp/block_store --network=iroha-network -e KEY="$nombre" hyperledger/iroha:latest
#Revisar que esta EMPTY y zzz
docker logs iroha

echo "Nombre del contenedor desplegado: iroha"
echo "Puerto del nodo: $puerto_interno"
echo "Puerto de consultas (servicio Torii): $puerto_torii"
echo "Para acceder al nodo interno o el servicio Torii necesitas saber la IP de tu dispositivo. Consultala con el comando 'ifconfig' (en Linux)"



