from iroha import Iroha, IrohaCrypto, IrohaGrpc



print("Introduce the host of main blockchain: ", end="")
hostgeneral = input()
print("Introduce the Torii port of main blockchain: ", end="")
portgeneral = input()
print("Introduce the host of patient blockchain to add: ", end="")
hostpaciente = input()
print("Introduce the Torii port of patient blockchain to add: ", end="")
portpaciente = input()
print("Introduce the id of patient: ", end="")
idpaciente = input()




iroha = Iroha('admin@test')
net = IrohaGrpc(str(hostgeneral)+':'+str(portgeneral))

admin_key = 'f101537e319568c765b2cc89698325604991dca57b9716b58016b253506cab70' #IrohaCrypto.private_key()
admin_tx = iroha.transaction(
 [iroha.command(
     'TransferAsset',
     src_account_id='admin@test',
     dest_account_id='test@test',
     asset_id="paciente"+str(idpaciente)+'#test',
     description=str(hostpaciente)+'##'+str(portpaciente),#+'##'+str('f101537e319568c765b2cc89698325604991dca57b9716b58016b253506cab70')
     amount='1'
 )]
)

IrohaCrypto.sign_transaction(admin_tx, admin_key)
net.send_tx(admin_tx)

for status in net.tx_status_stream(admin_tx):
    print(status)

