echo "Introduce el id del paciente al que pertenece el nodo: "
read id_paciente

echo "Introduce el puerto para las consultas Torii: "
read puerto_torii

echo "Introduce el puerto interno para el nodo de blockchain: "
read puerto_interno

echo "Introduce el puerto para gestionar la BD del nodo de blockchain: "
read puerto_bd



docker stop iroha-$id_paciente 
docker rm iroha-$id_paciente

docker stop postgres-$id_paciente
docker rm postgres-$id_paciente

docker volume rm blockstore-$id_paciente


docker network create iroha-network

#Revisar el nombre y el puerto
docker run --name postgres-$id_paciente -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=mysecretpassword -p $puerto_bd:5432 --network=iroha-network -d postgres:9.5 -c 'max_prepared_transactions=100'
#Revisar el nombre
docker volume create blockstore-$id_paciente


#Revisar el nombre, el puerto y el blockstore
docker run --name iroha-$id_paciente -d -p $puerto_torii:50051 -p $puerto_interno:10001 -v $(pwd)/iroha/example:/opt/iroha_data -v blockstore-$id_paciente:/tmp/block_store-$id_paciente --network=iroha-network -e KEY="node_$id_paciente" hyperledger/iroha:latest
#Revisar que esta EMPTY y zzz
docker logs iroha-$id_paciente

echo "Nombre del contenedor desplegado: iroha-$id_paciente"
echo "Puerto del nodo: $puerto_interno"
echo "Puerto de consultas (servicio Torii): $puerto_torii"
echo "Puerto de la BD interna: $puerto_bd"
echo "Para acceder al nodo interno o el servicio Torii necesitas saber la IP de tu dispositivo. Consultala con el comando 'ifconfig' (en Linux)"


